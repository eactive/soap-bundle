<?php

namespace Eactive\SoapBundle\DataCollector;

use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Eactive\SoapBundle\Logger\SoapLogger;

/**
 * @author Ger Jan van den Bosch <gerjan@e-active.nl>
 */
class SoapDataCollector extends DataCollector
{
    /**
     * @var SoapLogger
     */
    protected $logger;

    /**
     * @param SoapLogger $logger
     */
    public function __construct(SoapLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param \Exception $exception
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        if ($this->logger !== null) {
            $this->data = array(
                'calls' => $this->logger->getData(),
            );
        }
    }

    /**
     * @return int
     */
    public function getCalls()
    {
        return $this->data['calls'];
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return count($this->data['calls']);
    }

    /**
     * @return int
     */
    public function getTime()
    {
        $time = 0;
        foreach ($this->data['calls'] as $call) {
            $time += isset($call['duration']) ? $call['duration'] : 0;
        }
        return $time;
    }

    /**
     * @return int
     */
    public function getFaults()
    {
        $invalid = 0;
        foreach ($this->data['calls'] as $call) {
            if ($call['fault'] !== false) {
                $invalid++;
            }
        }
        return $invalid;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'soap';
    }
}
