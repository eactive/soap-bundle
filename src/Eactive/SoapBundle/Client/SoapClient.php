<?php

namespace Eactive\SoapBundle\Client;

use Eactive\SoapBundle\Logger\SoapLoggerInterface;

/**
 * @author Ger Jan van den Bosch <gerjan@e-active.nl>
 */
class SoapClient extends \SoapClient
{
    /**
     * @var SoapLoggerInterface | null
     */
    protected $logger;

    /**
     * @var string
     */
    protected $wsdl;

    /**
     * @param string $wsdl
     * @param array $options
     */
    public function __construct($wsdl, array $options = null)
    {
        $this->wsdl = $wsdl;
        $options['trace'] = 1;

        parent::__construct($wsdl, $options);
    }

    /**
     * @param string $function
     * @param array $arguments
     * @return object
     * @throws \SoapFault
     */
    public function __call($function, $arguments)
    {
        if ($this->logger) {
            $this->logger->startCall($this->wsdl, $function, $arguments);
        }

        $fault = false;

        try {
            $response = parent::__call($function, $arguments);
        } catch (\SoapFault $fault) {
            $response = null;

            if ($this->logger) {
                $this->logger->setFault($fault);
            }
        }

        if ($this->logger) {
            $this->logger->stopCall($response, parent::__getLastResponse(), parent::__getLastRequest());
        }

        if ($fault) {
            throw $fault;
        }

        return $response;
    }

    /**
     * @param SoapLoggerInterface $logger
     */
    public function setLogger(SoapLoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
