<?php

namespace Eactive\SoapBundle\Twig\Extension;

/**
 * @author Ger Jan van den Bosch <gerjan@e-active.nl>
 */
class FormatExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('json_beautify', array($this, 'beautifyJson')),
            new \Twig_SimpleFilter('xml_beautify', array($this, 'beautifyXml')),
        );
    }

    /**
     * @param string|array $json
     * @return string
     */
    public function beautifyJson($json)
    {
        if (is_array($json) || is_object($json)) {
            $json = json_encode($json);
        }

        // make sure every json output is the same
        $json = trim(trim($json, '['), ']');

        // http://snipplr.com/view/60559/prettyjson

        $result      = '';
        $pos         = 0;
        $strLen      = strlen($json);
        $indentStr   = '   ';
        $newLine     = "\n";
        $prevChar    = '';
        $outOfQuotes = true;

        for ($i=0; $i<=$strLen; $i++) {
            // Grab the next character in the string.
            $char = substr($json, $i, 1);

            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\') {
                $outOfQuotes = !$outOfQuotes;

                // If this character is the end of an element,
                // output a new line and indent the next line.
            } elseif (($char == '}' || $char == ']') && $outOfQuotes) {
                $result .= $newLine;
                $pos --;
                for ($j=0; $j<$pos; $j++) {
                    $result .= $indentStr;
                }
            }

            // Add the character to the result string.
            $result .= $char;

            // If the last character was the beginning of an element,
            // output a new line and indent the next line.
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                $result .= $newLine;
                if ($char == '{' || $char == '[') {
                    $pos ++;
                }

                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            $prevChar = $char;
        }

        $result = str_replace('":', '": ', $result);
        return $result;
    }

    /**
     * @param string $xml
     * @return string
     */
    public function beautifyXml($xml)
    {
        if (!empty($xml)) {
            $dom = new \DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->loadXML($xml);
            $dom->formatOutput = true;
            return $dom->saveXml();
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'format_extension';
    }
}
