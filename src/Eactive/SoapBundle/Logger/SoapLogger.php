<?php

namespace Eactive\SoapBundle\Logger;

use Symfony\Component\Stopwatch\Stopwatch;

/**
 * @author Ger Jan van den Bosch <gerjan@e-active.nl>
 */
class SoapLogger implements SoapLoggerInterface
{
    /**
     * @var string
     */
    protected $token;

    /**
     * @var Stopwatch
     */
    protected $stopwatch;

    /**
     * @var array
     */
    protected $data = array();

    /**
     */
    public function __construct()
    {
        $this->stopwatch = new Stopwatch();
    }

    /**
     * {@inheritdoc}
     */
    public function startCall($wsdl, $function, $arguments)
    {
        $this->token = sha1(uniqid(mt_rand(), true));

        $this->stopwatch->openSection();
        $this->stopwatch->start($this->token);

        $this->addData(array(
            'wsdl'      => $wsdl,
            'function'  => $function,
            'arguments' => $arguments,
            'fault'     => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function stopCall($response, $rawResponse = null, $rawRequest = null)
    {
        $this->stopwatch->stopSection($this->token);

        /** @var \Symfony\Component\Stopwatch\StopwatchEvent $event */
        $event = current($this->stopwatch->getSectionEvents($this->token));

        $this->addData(array(
            'response'    => $response,
            'rawResponse' => $rawResponse,
            'rawRequest'  => $rawRequest,
            'duration'    => $event->getDuration(), // in milliseconds
        ));
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function addData(array $data)
    {
        if (isset($this->data[$this->token])) {
            $data = array_merge($this->data[$this->token], $data);
        }

        $this->data[$this->token] = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * {@inheritdoc}
     */
    public function setFault(\SoapFault $fault)
    {
        $this->data[$this->token]['fault'] = $fault->getMessage();
        return $this;
    }
}
