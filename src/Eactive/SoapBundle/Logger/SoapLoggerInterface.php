<?php

namespace Eactive\SoapBundle\Logger;

/**
 * @author Ger Jan van den Bosch <gerjan@e-active.nl>
 */
interface SoapLoggerInterface
{
    /**
     * @param string $wsdl
     * @param string $function
     * @param array $arguments
     */
    public function startCall($wsdl, $function, $arguments);

    /**
     * @param object $response
     * @param string $rawResponse
     * @param string $rawRequest
     */
    public function stopCall($response, $rawResponse = null, $rawRequest = null);

    /**
     * @param \SoapFault $fault
     * @return $this
     */
    public function setFault(\SoapFault $fault);
}
